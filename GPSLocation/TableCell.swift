//
//  TableCell.swift
//  GPSLocation
//
//  Created by JEMS on 30/12/15.
//  Copyright © 2015 Tridentnets. All rights reserved.
//

import UIKit
import CoreData

class TableCell: UITableViewCell {

    @IBOutlet var Shadow_View: UIView!
   
    @IBOutlet var category_Lbl: UILabel!
    @IBOutlet var Sub_Name_Lbl: UILabel!
    @IBOutlet var Title_Lbl: UILabel!
    @IBOutlet var pincode_Lbl: UILabel!
    @IBOutlet var photo_Image: UIImageView!
    @IBOutlet var end_Name_Lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.frame.size.height-10)
        
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
