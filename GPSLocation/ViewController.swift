//
//  ViewController.swift
//  GPSLocation
//
//  Created by JEMS on 30/12/15.
//  Copyright © 2015 Tridentnets. All rights reserved.
//

import UIKit
import AVFoundation
import CoreLocation
import CoreData



class ViewController: UIViewController, CLLocationManagerDelegate
{
    
    
     var logItems = [MyLogs]()
    
     let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    @IBOutlet var newImage: UIImageView!
    @IBOutlet var capturedImage: UIImageView!
    @IBOutlet var previewView: UIView!
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
   
    @IBOutlet var Take_Photos: UIButton!
    
    var viewHasMovedToRight = false

    let locationManager = CLLocationManager()
    
    var image = UIImage()
    
    var requestBodyData: NSData!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBarHidden = true
        
        self .currentLocationsMethod()
        
     capturedImage.hidden=true
        
       
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        Take_Photos.layer.cornerRadius = 8.0
        Take_Photos.layer.masksToBounds = true
        
        
        captureSession = AVCaptureSession()
        captureSession!.sessionPreset = AVCaptureSessionPresetPhoto
        
        let backCamera = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: backCamera)
        } catch let error1 as NSError {
            error = error1
            input = nil
        }
        
        if error == nil && captureSession!.canAddInput(input) {
            captureSession!.addInput(input)
            
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            if captureSession!.canAddOutput(stillImageOutput) {
                captureSession!.addOutput(stillImageOutput)
                
                
                let bounds = previewView!.bounds
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
                previewLayer?.frame = CGRectMake(CGRectGetMinX(bounds), CGRectGetMinY(bounds), previewView.frame.width, previewView.frame.height)
                previewView.layer.addSublayer(previewLayer!)
                
                /*previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                previewLayer!.videoGravity = AVLayerVideoGravityResizeAspect
                previewView.layer.addSublayer(previewLayer!)*/
                captureSession!.startRunning()
                
                
            }
        }
        
        
        let orientation: UIDeviceOrientation = UIDevice.currentDevice().orientation
        print(orientation)
        
        switch (orientation)
        {
        case .Portrait:
            previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.Portrait
            break
        case .LandscapeRight:
            previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.LandscapeLeft
            break
        case .LandscapeLeft:
            previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.LandscapeRight
            break
        default:
            previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.Portrait
            break
        }

    }
    
    override func viewWillLayoutSubviews()
    {
        let orientation: UIDeviceOrientation = UIDevice.currentDevice().orientation
        print(orientation)
        
        switch (orientation)
        {
        case .Portrait:
            previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.Portrait
            break
        case .LandscapeRight:
            previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.LandscapeLeft
            break
        case .LandscapeLeft:
            previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.LandscapeRight
            break
        default:
            previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.Portrait
            break
        }
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        //previewLayer!.frame = CGRectMake(0, 0, previewView.frame.size.width, previewView.frame.size.height)
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            if placemarks!.count > 0 {
                let pm = placemarks![0] as CLPlacemark?
                
                if self.viewHasMovedToRight == false
                {
                    self.viewHasMovedToRight=true
                }
                else
                {
                    self.viewHasMovedToRight=false
                     self.displayLocationInfo(pm)
                }
                
               
                
            } else {
                print("Problem with the data received from geocoder")
            }
        })
    }
    
    func displayLocationInfo(placemark: CLPlacemark?)
    {
        if let containsPlacemark = placemark
        {
           
            self.locationManager.stopUpdatingLocation()
            
            let locality = (containsPlacemark.locality != nil) ? containsPlacemark.locality : ""
            let postalCode = (containsPlacemark.postalCode != nil) ? containsPlacemark.postalCode : ""
            let administrativeArea = (containsPlacemark.administrativeArea != nil) ? containsPlacemark.administrativeArea : ""
            let country = (containsPlacemark.country != nil) ? containsPlacemark.country : ""
            
            NSUserDefaults.standardUserDefaults().setObject(locality!, forKey: "locality")
            NSUserDefaults.standardUserDefaults().setObject(postalCode!, forKey: "postalCode")
            NSUserDefaults.standardUserDefaults().setObject(administrativeArea!, forKey: "administrativeArea")
            NSUserDefaults.standardUserDefaults().setObject(country!, forKey: "country")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            print(NSUserDefaults.standardUserDefaults().objectForKey("locality")!)
            print(NSUserDefaults.standardUserDefaults().objectForKey("postalCode")!)
            print(NSUserDefaults.standardUserDefaults().objectForKey("administrativeArea")!)
            print(NSUserDefaults.standardUserDefaults().objectForKey("country")!)
            
        }
        
            /*self.locationManager.stopUpdatingLocation()
            print(placemark!.locality)
            print(placemark!.postalCode)
            print(placemark!.administrativeArea)
            print(placemark!.country)*/
    }
    
    @IBAction func Take_Photo(sender: AnyObject)
    {
        if let videoConnection = stillImageOutput!.connectionWithMediaType(AVMediaTypeVideo) {
            
            stillImageOutput?.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: {(sampleBuffer, error) in
                if (sampleBuffer != nil) {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                    let dataProvider = CGDataProviderCreateWithCFData(imageData)
                    let cgImageRef = CGImageCreateWithJPEGDataProvider(dataProvider, nil, true, CGColorRenderingIntent.RenderingIntentDefault)
                    
                    
                    var imageOrientation : UIImageOrientation?
                    
                    switch UIDevice.currentDevice().orientation {
                        
                    case UIDeviceOrientation.PortraitUpsideDown:
                        imageOrientation = UIImageOrientation.Left
                    case UIDeviceOrientation.LandscapeRight:
                        imageOrientation = UIImageOrientation.Down
                    case UIDeviceOrientation.LandscapeLeft:
                        imageOrientation = UIImageOrientation.Up
                    case UIDeviceOrientation.Portrait:
                        imageOrientation = UIImageOrientation.Right
                    default:
                        imageOrientation = UIImageOrientation.Right
                        
                    }

                    self.image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: imageOrientation!)
                    self.capturedImage.image = self.image
                    
                    
                    
                    let imageSave = NSData(data: UIImageJPEGRepresentation(self.image, 1.0)!)
                    
                    let moc = self.managedObjectContext
                    
                    MyLogs.createInManagedObjectContext(moc,
                        title:NSUserDefaults.standardUserDefaults().objectForKey("locality")! as! String,
                        text: NSUserDefaults.standardUserDefaults().objectForKey("postalCode")! as! String,
                        area:NSUserDefaults.standardUserDefaults().objectForKey("administrativeArea")! as! String,
                        countrys: NSUserDefaults.standardUserDefaults().objectForKey("country")! as! String,
                        imageData: imageSave)
                    
                    
                    let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("userVC") as! userVC
                    
                    secondViewController.userData = imageSave
                    self.navigationController?.pushViewController(secondViewController, animated: true)

 
                
                }
            })
        }
        
        
    }
    
      func currentLocationsMethod()
    {
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error while updating location " + error.localizedDescription)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

