//
//  SecondVC.swift
//  GPSLocation
//
//  Created by JEMS on 30/12/15.
//  Copyright © 2015 Tridentnets. All rights reserved.
//

import UIKit
import CoreData
import BMCustomTableView
import RandomColorSwift

public var imagefortable : UIImage!


class SecondVC: UIViewController {

    
    @IBOutlet var tableViews: BMCustomTableView!
    
    var category_Text: String!
    
    var categoryArray: NSMutableArray = []
    
    
    var custom_cell :TableCell!
    var delete_button:UIButton?
    var UserLocation: String!
    var UserCode: String!
    var UserState: String!
    var UserCountry: String!
   @IBOutlet var photo: NSData!
    var UserLocationArray = [String]()
    var UserCodeArray = [String]()
    var UserStateArray = [String]()
    var UserCountryArray = [String]()
    var UserImageArray = [UIImage]()
    var people: [[String:Any]] = []
    var colors: [UIColor]!
    var logItems = [MyLogs]()
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    @IBAction func back_Btn(sender: AnyObject)
    {
        if let navController = self.navigationController {
            navController.popToRootViewControllerAnimated(true)
        }
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.navigationController?.navigationBarHidden = true
        self.tableViews.separatorStyle=UITableViewCellSeparatorStyle.None
        tableViews.backgroundColor=UIColor(red: 244.0/255, green: 244.0/255, blue: 244.0/255, alpha: 1.0)
         colors = randomColorsCount(20, hue: .Random, luminosity: .Light)
        
        fetchLog()
        
        categoryArray.addObject(category_Text)
        
        print(categoryArray)
    }
    
    func fetchLog() {
        let fetchRequest = NSFetchRequest(entityName: "MyLogs")
        if let fetchResults = (try? managedObjectContext.executeFetchRequest(fetchRequest)) as? [MyLogs] {
            logItems = fetchResults
            print(logItems)
        }
    }

    // MARK: - TableView data source
    
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
     {
        print(logItems.count)
        return logItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell!
    {
        custom_cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! TableCell
        let logs = logItems[indexPath.row]
        
        /*var mutableArray = logs.reverseObjectEnumerator().allObjects as! NSMutableArray*/
      
        custom_cell.Title_Lbl.text = logs.locality
        custom_cell.Sub_Name_Lbl.text = logs.postalCode
        custom_cell.pincode_Lbl.text = logs.administrativeArea
        custom_cell.end_Name_Lbl.text = logs.country
        custom_cell.photo_Image.image = UIImage(data: logs.imageCapture!)
        custom_cell.photo_Image.layer.cornerRadius = 10
        custom_cell.photo_Image.clipsToBounds = true
        //custom_cell.category_Lbl.text = categoryArray[indexPath.row] as? String
        
        
        custom_cell.Shadow_View.backgroundColor = colors[indexPath.row]
        custom_cell.Shadow_View.layer.cornerRadius = 10
        custom_cell.Shadow_View.clipsToBounds = true

        custom_cell.contentView.backgroundColor=UIColor.clearColor()
        
        delete_button = UIButton(frame: CGRectMake(CGRectGetWidth(custom_cell.frame)-60, 5, 50, 50))
        delete_button!.backgroundColor = UIColor.clearColor()
        let IMAGE = UIImage(named: "icon_close_circled")
         delete_button!.setImage(IMAGE, forState: UIControlState.Normal)
        //delete_button!.setTitle("Puzzle", forState: UIControlState.Normal)
        delete_button!.addTarget(self, action: "Delete_Btn_Action:", forControlEvents: UIControlEvents.TouchUpInside)
        delete_button!.tag = 22;
       //custom_cell.contentView.addSubview(delete_button!)
        
        let hover = createView(CGPoint(x: custom_cell.Shadow_View.frame.origin.x, y: custom_cell.Shadow_View.frame.origin.y), label: "Plain")
        applyPlainShadow(hover)

        return custom_cell
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        tableViews.customizeCell(custom_cell)
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       tableViews.deselectRowAtIndexPath(indexPath, animated: true)
        
        let logs = logItems[indexPath.row]
        
        let view = ImageModalView.instantiateFromNib()
        view.image = UIImage(data: logs.imageCapture!)
        let window = UIApplication.sharedApplication().delegate?.window
        let modal = PathDynamicModal.show(modalView: view, inView: window!!)
        view.closeButtonHandler = {[weak modal] in
            modal?.closeWithLeansRandom()
            return
        }
        view.bottomButtonHandler = {[weak modal] in
            modal?.closeWithLeansRandom()
            return
        }
    }
    func Delete_Btn_Action(sender:UIButton!)
    {
        let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        let context: NSManagedObjectContext = managedObjectContext
        let request = NSFetchRequest(entityName: "MyLog")
        request.returnsObjectsAsFaults = false
        
        do {
            let incidents = try context.executeFetchRequest(request)
            
            if incidents.count > 0 {
                
                for result: AnyObject in incidents{
                    context.deleteObject(result as! NSManagedObject)
                    print("NSManagedObject has been Deleted")
                }
                try context.save() } } catch {}
    }
    
    func createView(position: CGPoint, label: String) -> UIView {
        
        custom_cell.Shadow_View.frame = CGRect(origin: position, size: CGSize(width: custom_cell.Shadow_View.frame.size.width, height: custom_cell.Shadow_View.frame.size.height))
        //custom_cell.addSubview(custom_cell.Shadow_View);
        return custom_cell.Shadow_View
    }
    func applyHoverShadow(view: UIView) {
        let size = view.bounds.size
        let width = size.width
        let height = size.height
        
        let ovalRect = CGRect(x: 5, y: height + 5, width: width - 10, height: 15)
        let path = UIBezierPath(roundedRect: ovalRect, cornerRadius: 10)
        
        let layer = view.layer
        layer.shadowPath = path.CGPath
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: 0, height: 0)
    }

    func applyPlainShadow(view: UIView) {
        let layer = view.layer
        
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 5
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
