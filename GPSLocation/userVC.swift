//
//  userVC.swift
//  GPSLocation
//
//  Created by JEMS on 05/01/16.
//  Copyright © 2016 Tridentnets. All rights reserved.
//

import UIKit


class userVC: UIViewController, UIPopoverControllerDelegate, UIPopoverPresentationControllerDelegate, UIPickerViewDelegate
{
   
    @IBOutlet var imag_Frame: UIImageView!

    var typePickerView: UIPickerView = UIPickerView()
    
    @IBOutlet var Submits: UIButton!
     let typeView: UIViewController = UIViewController()
    
    var cutomView: UIView = UIView()
    
    var string:String!
    
    var seasonalItems: [String] = ["Spring", "Summer", "Fall", "Winter"]
    
    @IBOutlet var text_Big_View: UIView!
    
    var boolean : Bool?
    
    var userData : NSData = NSData()
    
    
    @IBOutlet var text_User: UITextField!
    
    @IBOutlet var cancels_Btn: UIButton!
    
    @IBAction func Cancel_Btn(sender: AnyObject)
    {
        self.navigationController!.popToRootViewControllerAnimated(true)
    }
    @IBAction func Submit(sender: AnyObject)
    {
        
        if text_User.text != ""
        {
            let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("SecondVC") as! SecondVC
            
            secondViewController.category_Text = text_User.text
            self.navigationController?.pushViewController(secondViewController, animated: true)
            
        }
        else
        {
            let alertView = UNAlertView(title: "GPSLocation", message: "Please choose the categories")
            
            alertView.messageAlignment = NSTextAlignment.Center
            alertView.buttonAlignment  = UNButtonAlignment.Horizontal
            alertView.titleFont   = UIFont(name: "Avenir-Next-Bold", size: 20)
            alertView.messageFont = UIFont(name: "Avenir-Next", size: 16)
            alertView.addButton("Yes", backgroundColor: UIColor .orangeColor(), action: {
                print("Yes action")
            })

            // Show
             alertView.show()
        }
        
      }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        imag_Frame.layer.cornerRadius = 10
        imag_Frame.clipsToBounds = true
        /*imag_Frame.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        imag_Frame.layer.shadowColor = UIColor.yellowColor().CGColor
        imag_Frame.layer.shadowRadius = 5.0
        imag_Frame.image = UIImage(data: userData)*/
        imag_Frame.layer.shadowOpacity = 0.7
        
        
        text_Big_View.layer.cornerRadius = 8.0
        text_Big_View.layer.masksToBounds = true
        text_Big_View.layer.borderColor = UIColor.orangeColor().CGColor
        text_Big_View.layer.borderWidth = 2.0
        
        
        Submits.layer.cornerRadius = 8.0
        Submits.layer.masksToBounds = true
        
        cancels_Btn.layer.cornerRadius = 8.0
        cancels_Btn.layer.masksToBounds = true
        
       
        
        /*let plain = createView(CGPoint(x: imag_Frame.frame.origin.x, y: 50), label: "plain")
        applyPlainShadow(plain);
        
        let curved = createView(CGPoint(x: imag_Frame.frame.origin.x, y: 200), label: "curved")
        applyCurvedShadow(curved)*/
        
        let hover = createView(CGPoint(x: imag_Frame.frame.origin.x, y: imag_Frame.frame.origin.y), label: "hover")
        applyHoverShadow(hover)
        
        
        
    }
    
    func createView(position: CGPoint, label: String) -> UIView {
       
        imag_Frame.image = UIImage(data: userData)
        imag_Frame.frame = CGRect(origin: position, size: CGSize(width: imag_Frame.frame.size.width, height: imag_Frame.frame.size.height))
        return imag_Frame
    }
    func applyPlainShadow(view: UIView) {
        let layer = view.layer
        
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 5
    }
    
    func applyCurvedShadow(view: UIView) {
        let size = view.bounds.size
        let width = size.width
        let height = size.height
        let depth = CGFloat(11.0)
        let lessDepth = 0.8 * depth
        let curvyness = CGFloat(5)
        let radius = CGFloat(1)
        
        let path = UIBezierPath()
        
        // top left
        path.moveToPoint(CGPoint(x: radius, y: height))
        
        // top right
        path.addLineToPoint(CGPoint(x: width - 2*radius, y: height))
        
        // bottom right + a little extra
        path.addLineToPoint(CGPoint(x: width - 2*radius, y: height + depth))
        
        // path to bottom left via curve
        path.addCurveToPoint(CGPoint(x: radius, y: height + depth),
            controlPoint1: CGPoint(x: width - curvyness, y: height + lessDepth - curvyness),
            controlPoint2: CGPoint(x: curvyness, y: height + lessDepth - curvyness))
        
        let layer = view.layer
        layer.shadowPath = path.CGPath
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOpacity = 0.3
        layer.shadowRadius = radius
        layer.shadowOffset = CGSize(width: 0, height: -3)
    }
    
    func applyHoverShadow(view: UIView) {
        let size = view.bounds.size
        let width = size.width
        let height = size.height
        
        let ovalRect = CGRect(x: 5, y: height + 5, width: width - 10, height: 15)
        let path = UIBezierPath(roundedRect: ovalRect, cornerRadius: 10)
        
        let layer = view.layer
        layer.shadowPath = path.CGPath
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: 0, height: 0)
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return seasonalItems.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return seasonalItems[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        string = seasonalItems[row]
        
         //text_User.text = seasonalItems[row]
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        text_User.text = ""
        
    }
    
    override func viewDidAppear(animated: Bool) {
       super.viewDidAppear(animated)
    }
    
    // UITextField Delegates
    func textFieldDidBeginEditing(textField: UITextField)
    {
        print("TextField did begin editing method called")
        
       /* if seasonalItems.objectAtIndex(4)
        {
            text_User.text = "";
            
            boolean = true
            text_User .becomeFirstResponder()
        }
        else
        {
            boolean = false
        }*/
        
        
        if text_User == textField
        {
            
            if let i = seasonalItems.indexOf("Winter")
            {
                
                let popView: UIPopoverController = UIPopoverController(contentViewController: typeView)
                
                popView .dismissPopoverAnimated(true)
                
                text_User.text = "";
                boolean = true
                text_User.becomeFirstResponder()
                
                
                print("Jason is at index \(i)")
            } else
            {
               
                boolean = false
                print("Jason isn't in the array")
            }
        }
    
        
    }
    func textFieldDidEndEditing(textField: UITextField)
    {
        
        boolean = false
        
        print("TextField did end editing method called")
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        print("TextField should begin editing method called")
        if text_User == textField
        {
            if boolean == true
            {
                boolean = false
            }
            else
            {
                
                typeView.view = cutomView
                typeView.modalPresentationStyle = .Popover
                let popView: UIPopoverController = UIPopoverController(contentViewController: typeView)
                popView.delegate = self
                popView .presentPopoverFromRect(CGRectMake(800, 415, 0, 0), inView: self.view, permittedArrowDirections:UIPopoverArrowDirection.Up, animated: true)
                popView.popoverContentSize = CGSizeMake(300, 250)
                
                
                
                let buttonPuzzle:UIButton = UIButton(frame: CGRectMake(10, 10, 80, 30))
                buttonPuzzle.backgroundColor = UIColor.orangeColor()
                buttonPuzzle.setTitle("Cancel", forState: UIControlState.Normal)
                buttonPuzzle.addTarget(self, action: "Cancel_Btn_Action:", forControlEvents: UIControlEvents.TouchUpInside)
                buttonPuzzle.tag = 22;
                cutomView.addSubview(buttonPuzzle)
                
                let button_Done:UIButton = UIButton(frame: CGRectMake(buttonPuzzle.frame.origin.x+buttonPuzzle.frame.size.width+120, 10, 80, 30))
                button_Done.backgroundColor = UIColor.orangeColor()
                button_Done.setTitle("Done", forState: UIControlState.Normal)
                button_Done.addTarget(self, action: "Done_Btn_Action:", forControlEvents: UIControlEvents.TouchUpInside)
                button_Done.tag = 22;
                cutomView.addSubview(button_Done)
                
                self.typePickerView.delegate = self
                self.typePickerView.frame = CGRectMake(0, buttonPuzzle.frame.origin.y+buttonPuzzle.frame.size.height, 300, 216)
                self.typePickerView.backgroundColor = UIColor.clearColor()
                self.typePickerView.layer.borderColor = UIColor.whiteColor().CGColor
                self.typePickerView.layer.borderWidth = 1
                cutomView .addSubview(self.typePickerView)
                
                return false
                
            }
        }
        
        return true;
    }
    func textFieldShouldClear(textField: UITextField) -> Bool
    {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        print("TextField should snd editing method called")
        return true;
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    func Done_Btn_Action(sender:UIButton!)
    {
        /*var btnsendtag:UIButton = sender
        if btnsendtag.tag == 22 {
            //println("Button tapped tag 22")
        }*/
        
        let row = seasonalItems.count
        
        self.typePickerView .selectRow(row, inComponent: 0, animated: true)
        
        text_User.text = string
       
        self .dismissViewControllerAnimated(true, completion: nil)
        
        let quotation = text_User.text
        let sameQuotation = "Winter"
        if quotation == sameQuotation
        {
            let popView: UIPopoverController = UIPopoverController(contentViewController: typeView)
            popView .dismissPopoverAnimated(true)
            boolean = true
            text_User.becomeFirstResponder()
        }
    }
    
    func Cancel_Btn_Action(sender:UIButton!)
    {
        /*var btnsendtag:UIButton = sender
        if btnsendtag.tag == 22 {
        //println("Button tapped tag 22")
        }*/
        
        text_User.text = ""
        self .dismissViewControllerAnimated(true, completion: nil)
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
