//
//  MyLogs+CoreDataProperties.swift
//  
//
//  Created by JEMS on 07/01/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension MyLogs {

    @NSManaged var locality: String?
    @NSManaged var postalCode: String?
    @NSManaged var administrativeArea: String?
    @NSManaged var country: String?
    @NSManaged var categorys: String?
    @NSManaged var imageCapture: NSData?

}
