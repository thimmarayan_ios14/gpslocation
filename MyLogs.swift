//
//  MyLogs.swift
//  
//
//  Created by JEMS on 07/01/16.
//
//

import Foundation
import CoreData

class MyLogs: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    class func createInManagedObjectContext(moc: NSManagedObjectContext, title: String, text: String,area: String, countrys: String, imageData: NSData) -> MyLogs {
        let newItem = NSEntityDescription.insertNewObjectForEntityForName("MyLogs", inManagedObjectContext: moc) as! MyLogs
        newItem.locality = title
        newItem.postalCode = text
        newItem.administrativeArea=area
        newItem.country=countrys
        newItem.imageCapture = imageData
        return newItem
    }

    
    
    
}
